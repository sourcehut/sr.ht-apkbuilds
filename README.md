These are [Alpine Linux](https://alpinelinux.org) packages for sr.ht and any
dependencies which are not packaged in upstream Alpine Linux. The results are
published in our Alpine repository:

https://mirror.sr.ht/alpine/

Note that some sr.ht packages (meta.sr.ht, python-srht, etc) are automatically
built and published by builds.sr.ht. This process automatically updates the
pkgver and pkgrel, but does not commit it to the repository. If using these
APKBUILDs yourself, be aware that the version numbers here may be outdated.

## Alpine upgrade procedure

When a new Alpine version is released:

1. Update the Alpine version in build.yml
1. Update non-sr.ht packages to latest upstream version, reset pkgrel to 0
1. `./submit-build` everything _except_ for *.sr.ht
1. Update pkgver in *.sr.ht packages, commit & push & run `./submit-build`
1. Update build manifests upstream for *.sr.ht
1. Run updates on non-critical boxen
1. Run updates on all boxen

**Tips**

- Upgrade systems with apk upgrade -a
- Update the python version in the Makefiles
- Update the python version with nginx
