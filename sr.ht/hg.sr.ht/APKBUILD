# Maintainer: Drew DeVault <sir@cmpwn.com>
pkgname=hg.sr.ht
pkgver=0.35.0
pkgrel=0
pkgdesc="sr.ht mercurial service"
url="https://hg.sr.ht/~sircmpwn/hg.sr.ht"
arch="all"
license="AGPLv3"
depends="
	core.sr.ht
	hg-evolve
	py3-hglib
	py3-redis
	py3-scmsrht
	py3-unidiff
"
makedepends="
	core.sr.ht-dev
	py3-build
	py3-installer
	py3-setuptools
	py3-setuptools_scm
	py3-wheel
	sassc
	go
	minify
"
subpackages="
	$pkgname-openrc
	$pkgname-dbg
	$pkgname-dev:_dev:noarch
	$pkgname-nginx:_nginx:noarch
"
source="
	$pkgname-$pkgver.tar.gz::https://hg.sr.ht/~sircmpwn/$pkgname/archive/$pkgver.tar.gz
	sr.ht-nginx-$pkgver.tar.gz::https://git.sr.ht/~sircmpwn/sr.ht-nginx/archive/master.tar.gz
	hg.sr.ht.initd
	hg.sr.ht.confd
	hg.sr.ht-api.initd
	hg.sr.ht-api.confd
	hg.sr.ht-webhooks.initd
	hg.sr.ht-webhooks.confd
	hg.sr.ht.gunicorn.conf.py
"
export PKGVER=$pkgver
options="$options !check net"
install="$pkgname.pre-install $pkgname.post-upgrade"

build() {
	make PREFIX="/usr"
	python3 -m build --wheel -n
}

package() {
	make PREFIX="$pkgdir/usr" install
	python3 -m installer --destdir="$pkgdir" --compile-bytecode=1 dist/*.whl
	mkdir -p "$pkgdir"/etc/init.d
	install -Dm755 "$srcdir"/hg.sr.ht.initd \
		"$pkgdir"/etc/init.d/hg.sr.ht
	install -Dm644 "$srcdir"/hg.sr.ht.confd \
		"$pkgdir"/etc/conf.d/hg.sr.ht
	install -Dm755 "$srcdir"/hg.sr.ht-api.initd \
		"$pkgdir"/etc/init.d/hg.sr.ht-api
	install -Dm644 "$srcdir"/hg.sr.ht-api.confd \
		"$pkgdir"/etc/conf.d/hg.sr.ht-api
	install -Dm755 "$srcdir"/hg.sr.ht-webhooks.initd \
		"$pkgdir"/etc/init.d/hg.sr.ht-webhooks
	install -Dm644 "$srcdir"/hg.sr.ht-webhooks.confd \
		"$pkgdir"/etc/conf.d/hg.sr.ht-webhooks
	install -Dm644 "$srcdir"/hg.sr.ht.gunicorn.conf.py \
		"$pkgdir"/etc/sr.ht/hg.sr.ht.gunicorn.conf.py
	install -Dm644 hgsrht/hgrcs/global.cfg "$pkgdir"/etc/sr.ht/hgrcs/global.cfg
	install -Dm644 hgsrht/hgrcs/nonpublishing.cfg "$pkgdir"/etc/sr.ht/hgrcs/nonpublishing.cfg
}

_dev() {
        pkgdesc="hg.sr.ht dev assets"
        mkdir -p "$subpkgdir"/usr/share/sourcehut/
        mv "$pkgdir"/usr/share/sourcehut/hg.sr.ht.graphqls "$subpkgdir"/usr/share/sourcehut/
}

_nginx() {
	depends="sr.ht-nginx"
	pkgdesc="nginx configuration for $pkgname"
	install -Dm644 "$srcdir/sr.ht-nginx-master/hg.sr.ht.conf" \
		"$subpkgdir"/etc/nginx/http.d/hg.sr.ht.conf
}

sha512sums="
c3e3303a4b7a68707c96c5c6b4f5635d803d71e72fb6cd2819875a49cf57460137c6d5f74c8494d71c58db7aa5aef0695678d7ec98b8315dd4ae999c50adb7eb  hg.sr.ht-0.35.0.tar.gz
342803ee7954f53df359c1cc00f174bcca112370b141c21ff48db10717ab68c184d5047a1f62d73d2a6552fbae2e4261a61d3a8af34a09f8724d721e2ef43058  sr.ht-nginx-0.35.0.tar.gz
dbe9719284c79703ed9d45e2977e6c2bdb94faa553901b33c6f008b2a5da9a77ff6f53d2c2e79b4aac2da0078836796263a8cb6ef1f558d263296c3467971687  hg.sr.ht.initd
867dc537ea53099a2ab4dbc8c400012ca24053d9226281322c103944faa3b66a7986c325c860718882a4b28b986a2208f9a9b40f37a2d54c19be46ebe3902a82  hg.sr.ht.confd
aa7a4a9792427f32eff7ae027554b8c6884f5e5f168bc53ebe60ccea24cc40728ac68b5ecbd095c66e9d471196b26329f6dedb9b46648d2c63011be60128b100  hg.sr.ht-api.initd
eaed280fc25b88294c5d02893e72b8751afc1db0db3e5f20123239ca28ae6f3e53b22c11992e44a4f8b4943f6326da0ed03ad0bd0a900d10efcb507d35b1b64f  hg.sr.ht-api.confd
93f25762da77462c8167ae53ba37ab4f9982c7f3c2d03dde445f0578758b808987c4781912a0fa5b4a4e25ba50c9d27477dfea8e319908ddc124dc836aefd2d6  hg.sr.ht-webhooks.initd
af5bb605d8ba2e848de5755c9c7f1607591068f6ac79ddc1d312873e92218c6b535eb4cbcf10590656239459b7808ba5469d52d32f80e8e2448fc18c22f7a70c  hg.sr.ht-webhooks.confd
39c12898976aa3b5896ed8287216cf4f2ed4998722891e1b5e507070df86f9111286396eb82cc01b3faaf1016b51db47d077fc4b1b7d72e3782f936c89fb3e37  hg.sr.ht.gunicorn.conf.py
"
