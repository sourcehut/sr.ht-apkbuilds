from prometheus_client import multiprocess
import traceback
import io

def worker_abort(worker):
    debug_info = io.StringIO()
    debug_info.write("Traceback at time of timeout:\n")
    traceback.print_stack(file=debug_info)
    worker.log.critical(debug_info.getvalue())

def child_exit(server, worker):
    multiprocess.mark_process_dead(worker.pid)
