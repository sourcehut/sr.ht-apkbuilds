# Maintainer: Drew DeVault <sir@cmpwn.com>
pkgname=man.sr.ht
pkgver=0.16.6
pkgrel=0
pkgdesc="sr.ht wiki service"
url="https://git.sr.ht/~sircmpwn/man.sr.ht"
arch="all"
license="AGPLv3"
depends="
	core.sr.ht
	py3-gunicorn
	py3-pygit2
	py3-srht
	py3-scmsrht
	py3-yaml
"
makedepends="
	core.sr.ht-dev
	py3-build
	py3-installer
	py3-setuptools
	py3-setuptools_scm
	py3-wheel
	sassc
	go
	minify
"
subpackages="
	$pkgname-openrc
	$pkgname-dbg
	$pkgname-dev:_dev:noarch
	$pkgname-nginx:_nginx:noarch
"
source="
	$pkgname-$pkgver.tar.gz::https://git.sr.ht/~sircmpwn/$pkgname/archive/$pkgver.tar.gz
	sr.ht-nginx-$pkgver.tar.gz::https://git.sr.ht/~sircmpwn/sr.ht-nginx/archive/master.tar.gz
	man.sr.ht.initd
	man.sr.ht.confd
	man.sr.ht-api.initd
	man.sr.ht-api.confd
	man.sr.ht.gunicorn.conf.py
"
export PKGVER=$pkgver
options="$options !check net"
install="$pkgname.pre-install $pkgname.post-upgrade"

build() {
	make PREFIX="/usr"
	python3 -m build --wheel -n
}

package() {
	make PREFIX="$pkgdir/usr" install
	python3 -m installer --destdir="$pkgdir" --compile-bytecode=1 dist/*.whl
	install -Dm755 "$srcdir"/man.sr.ht.initd \
		"$pkgdir"/etc/init.d/man.sr.ht
	install -Dm644 "$srcdir"/man.sr.ht.confd \
		"$pkgdir"/etc/conf.d/man.sr.ht
	install -Dm644 "$srcdir"/man.sr.ht.gunicorn.conf.py \
		"$pkgdir"/etc/sr.ht/man.sr.ht.gunicorn.conf.py

	install -Dm755 "$srcdir"/man.sr.ht-api.initd \
		"$pkgdir"/etc/init.d/man.sr.ht-api
	install -Dm644 "$srcdir"/man.sr.ht-api.confd \
		"$pkgdir"/etc/conf.d/man.sr.ht-api
}

_dev() {
        pkgdesc="man.sr.ht dev assets"
        mkdir -p "$subpkgdir"/usr/share/sourcehut/
        mv "$pkgdir"/usr/share/sourcehut/man.sr.ht.graphqls "$subpkgdir"/usr/share/sourcehut/
}

_nginx() {
	depends="sr.ht-nginx"
	pkgdesc="nginx configuration for $pkgname"
	install -Dm644 "$srcdir/sr.ht-nginx-master/man.sr.ht.conf" \
		"$subpkgdir"/etc/nginx/http.d/man.sr.ht.conf
}

sha512sums="
92918d1bd27a6ce7c704846801e73f18678e12a3bf4f95864528b7b978eebe2e908bb98b5921276d9e79783435f876e257e35edd70ade60638be8c38199acc34  man.sr.ht-0.16.6.tar.gz
efa7b349a699c80e7c04c4ef53d164ecf656b0d37407b65a730203b581b039079fa31dfa1bd7b59c4e21e1983fdf6994473eddfa3fb0b150271c8d0e8c1def39  sr.ht-nginx-0.16.6.tar.gz
626300024baaf62c21152de76ab4256ed38a699d2a2c30b4758a115dc8e14ab2986dc9ef5d3ba77e64261ef5cd121342212c3621cfe47b7a20e80dca2247480d  man.sr.ht.initd
2a271c656ab5e4314142200f8c9d4d95fd6a9080d561a64fcfa9530e258168f56a863e759400120f7b0ed98f2212a19e3e8afe4e2d4fab53071e88146a5fac08  man.sr.ht.confd
5c1b7726319a2a32cb462b36b247e621cd9b035572b3dd87fc3d1c7642bdff4a89e359f091c00e8143c71c392e1ac8c532ac312fabb3f276d61002ffd7660789  man.sr.ht-api.initd
8f935978f6e70a82dc746702e8aded6bfb956b4f00d5cb03529ada95e8f6e5b7c98df4e47e52ce394705cb251abc6b09ecd267025ea3165278f29f8884a767da  man.sr.ht-api.confd
39c12898976aa3b5896ed8287216cf4f2ed4998722891e1b5e507070df86f9111286396eb82cc01b3faaf1016b51db47d077fc4b1b7d72e3782f936c89fb3e37  man.sr.ht.gunicorn.conf.py
"
