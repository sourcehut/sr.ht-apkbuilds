# Maintainer: Drew DeVault <sir@cmpwn.com>
pkgname=paste.sr.ht
pkgver=0.15.5
pkgrel=0
pkgdesc="sr.ht paste service"
url="https://git.sr.ht/~sircmpwn/paste.sr.ht"
arch="all"
license="AGPLv3"
depends="
	core.sr.ht
	py3-gunicorn
	py3-srht
	py3-yaml
"
makedepends="
	core.sr.ht-dev
	py3-build
	py3-installer
	py3-setuptools
	py3-setuptools_scm
	py3-wheel
	sassc
	minify
	go
"
subpackages="
	$pkgname-openrc
	$pkgname-dbg
	$pkgname-dev:_dev:noarch
	$pkgname-nginx:_nginx:noarch
"
source="
	$pkgname-$pkgver.tar.gz::https://git.sr.ht/~sircmpwn/$pkgname/archive/$pkgver.tar.gz
	sr.ht-nginx-$pkgver.tar.gz::https://git.sr.ht/~sircmpwn/sr.ht-nginx/archive/master.tar.gz
	paste.sr.ht.initd
	paste.sr.ht.confd
	paste.sr.ht-api.initd
	paste.sr.ht-api.confd
	paste.sr.ht.gunicorn.conf.py
"
export PKGVER=$pkgver
options="$options !check net"
install="$pkgname.pre-install $pkgname.post-upgrade"

build() {
	make PREFIX="/usr"
	python3 -m build --wheel -n
}

package() {
	make PREFIX="$pkgdir/usr" install
	python3 -m installer --destdir="$pkgdir" --compile-bytecode=1 dist/*.whl
	install -Dm755 "$srcdir"/paste.sr.ht.initd \
		"$pkgdir"/etc/init.d/paste.sr.ht
	install -Dm644 "$srcdir"/paste.sr.ht.confd \
		"$pkgdir"/etc/conf.d/paste.sr.ht
	install -Dm755 "$srcdir"/paste.sr.ht-api.initd \
		"$pkgdir"/etc/init.d/paste.sr.ht-api
	install -Dm644 "$srcdir"/paste.sr.ht-api.confd \
		"$pkgdir"/etc/conf.d/paste.sr.ht-api
	install -Dm644 "$srcdir"/paste.sr.ht.gunicorn.conf.py \
		"$pkgdir"/etc/sr.ht/paste.sr.ht.gunicorn.conf.py
}

_dev() {
        pkgdesc="meta.sr.ht dev assets"
        mkdir -p "$subpkgdir"/usr/share/sourcehut/
        mv "$pkgdir"/usr/share/sourcehut/paste.sr.ht.graphqls "$subpkgdir"/usr/share/sourcehut/
}

_nginx() {
	depends="sr.ht-nginx"
	pkgdesc="nginx configuration for $pkgname"
	install -Dm644 "$srcdir/sr.ht-nginx-master/paste.sr.ht.conf" \
		"$subpkgdir"/etc/nginx/http.d/paste.sr.ht.conf
}

sha512sums="
cb11058322f5792bdafcb8dc9f80c5f55ff118bc0fbd32d935a62249f3d6059d8d64593fc8c3528b9a1a21fbe326d65869d62522c4f9ba79e272af38c3eae403  paste.sr.ht-0.15.5.tar.gz
efa7b349a699c80e7c04c4ef53d164ecf656b0d37407b65a730203b581b039079fa31dfa1bd7b59c4e21e1983fdf6994473eddfa3fb0b150271c8d0e8c1def39  sr.ht-nginx-0.15.5.tar.gz
c6cf45c795f82493a2779c12d67af3ae934d171c4ec81e1a4d36c411b8ff7234bc6ba396a411781e9dfc861557a9223066b5342b5fa9908d1b1bf08f1a12466f  paste.sr.ht.initd
dea54d754186f6bc67c8ca3ef6cc5236355cdc9962aa502d4138245bdcba7edb804b627fcfbf4909fc54bed08ef44ce635947982ff3cb69f3dac0bb6bd55abb3  paste.sr.ht.confd
3c858c5854c1302c9183d7db75c6fcf667a8629a6a6f2373177086b94b3efaef93e147e2e851faac516dc1f1148f927ac9e5be846fd8b72d3e746e3a7c940167  paste.sr.ht-api.initd
e226a1a3913749c5a72e57c2e6f75f94199f47c72d47ccbac5abba3bc30b827d8619c2f1a60ef2b67f28074007a051981f47d4d2c749da4c5e5e9d1823f54dbf  paste.sr.ht-api.confd
39c12898976aa3b5896ed8287216cf4f2ed4998722891e1b5e507070df86f9111286396eb82cc01b3faaf1016b51db47d077fc4b1b7d72e3782f936c89fb3e37  paste.sr.ht.gunicorn.conf.py
"
