# Contributor: Fabian Affolter <fabian@affolter-engineering.ch>
# Maintainer: Fabian Affolter <fabian@affolter-engineering.ch>
pkgname=py3-ansi2html
_pyname=ansi2html
pkgver=1.9.2
pkgrel=0
pkgdesc="Convert text with ANSI color codes to HTML"
url="http://github.com/ralphbean/ansi2html/"
arch="noarch"
license="LGPL-3.0-or-later"
depends="py3-six"
makedepends="py3-setuptools_scm py3-wheel py3-installer py3-build"
checkdepends="py3-pytest py3-mock py3-nose"
replaces="py-ansi2html py2-ansi2html"
_pypiprefix="${_pyname%${_pyname#?}}"
source="https://files.pythonhosted.org/packages/source/$_pypiprefix/$_pyname/$_pyname-$pkgver.tar.gz"
builddir="$srcdir"/$_pyname-$pkgver
options="!check" # Upstream tests are stupid

prepare() {
	sed -i pyproject.toml -e 's/.*setuptools_scm_git_archive.*//g'
}

build() {
	export SETUPTOOLS_SCM_PRETEND_VERSION=$pkgver
	python3 -m build --no-isolation --wheel .
}

check() {
	PYTHONPATH="$(pwd)" pytest-3
}

package() {
	python3 -m installer --destdir="$pkgdir" \
		dist/ansi2html-$pkgver-py3-none-any.whl
}

sha512sums="
c465259d4c9ea82725bcd8cd286246b806b1bfde06f89b52b1cc3cd5c514be92a19ffcfd147417d64ce6da6627678baabc7ff05226ed392343cdb0496bd375eb  ansi2html-1.9.2.tar.gz
"
