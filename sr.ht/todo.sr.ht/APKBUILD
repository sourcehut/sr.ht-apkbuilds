# Maintainer: Drew DeVault <sir@cmpwn.com>
pkgname=todo.sr.ht
pkgver=0.77.0
pkgrel=0
pkgdesc="sr.ht ticket tracking service"
url="https://git.sr.ht/~sircmpwn/todo.sr.ht"
arch="all"
license="AGPLv3"
depends="
	core.sr.ht
	py3-aiosmtpd
	py3-gunicorn
	py3-srht
"
makedepends="
	core.sr.ht-dev
	py3-build
	py3-installer
	py3-setuptools
	py3-setuptools_scm
	py3-wheel
	sassc
	go
	minify
"
subpackages="
	$pkgname-openrc
	$pkgname-dbg
	$pkgname-dev:_dev:noarch
	$pkgname-nginx:_nginx:noarch
"
source="
	$pkgname-$pkgver.tar.gz::https://git.sr.ht/~sircmpwn/$pkgname/archive/$pkgver.tar.gz
	sr.ht-nginx-$pkgver.tar.gz::https://git.sr.ht/~sircmpwn/sr.ht-nginx/archive/master.tar.gz
	todo.sr.ht.initd
	todo.sr.ht.confd
	todo.sr.ht-lmtp.initd
	todo.sr.ht-lmtp.confd
	todo.sr.ht-webhooks.confd
	todo.sr.ht-webhooks.initd
	todo.sr.ht-api.confd
	todo.sr.ht-api.initd
	todo.sr.ht.gunicorn.conf.py
"
export PKGVER=$pkgver
options="$options !check net"
install="$pkgname.pre-install $pkgname.post-upgrade"

build() {
	make PREFIX="/usr"
	python3 -m build --wheel -n
}

package() {
	make PREFIX="$pkgdir/usr" install
	python3 -m installer --destdir="$pkgdir" --compile-bytecode=1 dist/*.whl

	install -Dm755 "$srcdir"/todo.sr.ht.initd \
		"$pkgdir"/etc/init.d/todo.sr.ht
	install -Dm644 "$srcdir"/todo.sr.ht.confd \
		"$pkgdir"/etc/conf.d/todo.sr.ht
	install -Dm755 "$srcdir"/todo.sr.ht-lmtp.initd \
		"$pkgdir"/etc/init.d/todo.sr.ht-lmtp
	install -Dm644 "$srcdir"/todo.sr.ht-lmtp.confd \
		"$pkgdir"/etc/conf.d/todo.sr.ht-lmtp
	install -Dm755 "$srcdir"/todo.sr.ht-webhooks.initd \
		"$pkgdir"/etc/init.d/todo.sr.ht-webhooks
	install -Dm644 "$srcdir"/todo.sr.ht-webhooks.confd \
		"$pkgdir"/etc/conf.d/todo.sr.ht-webhooks
	install -Dm755 "$srcdir"/todo.sr.ht-api.initd \
		"$pkgdir"/etc/init.d/todo.sr.ht-api
	install -Dm644 "$srcdir"/todo.sr.ht-api.confd \
		"$pkgdir"/etc/conf.d/todo.sr.ht-api
	install -Dm644 "$srcdir"/todo.sr.ht.gunicorn.conf.py \
		"$pkgdir"/etc/sr.ht/todo.sr.ht.gunicorn.conf.py
}

_dev() {
        pkgdesc="meta.sr.ht dev assets"
        mkdir -p "$subpkgdir"/usr/share/sourcehut/
        mv "$pkgdir"/usr/share/sourcehut/todo.sr.ht.graphqls "$subpkgdir"/usr/share/sourcehut/
}

_nginx() {
	depends="sr.ht-nginx"
	pkgdesc="nginx configuration for $pkgname"
	install -Dm644 "$srcdir/sr.ht-nginx-master/todo.sr.ht.conf" \
		"$subpkgdir"/etc/nginx/http.d/todo.sr.ht.conf
}

sha512sums="
232bd04d94db165988f42a6f71a25c5b391c3769bdefba88cca3ffe68f165b8a5d88406f2cf52954270ccdbb8d8c89366dbfdca523e16dca65317c462535fb0b  todo.sr.ht-0.77.0.tar.gz
342803ee7954f53df359c1cc00f174bcca112370b141c21ff48db10717ab68c184d5047a1f62d73d2a6552fbae2e4261a61d3a8af34a09f8724d721e2ef43058  sr.ht-nginx-0.77.0.tar.gz
ba08b31d7be31a0d3ab6acbeed3881aaa33eb7f1e086153fa7722fb9e292c04fb846f2b5032d9cffdfce55bcd5d2cfa2590bcb58be8915d9d371920d5cac483e  todo.sr.ht.initd
a71116bd25e0c6e8e3e14e4dadb653e6360e2b468bd415439559b56ffc18724efa9bcb593181d8f0fe0cbccf96f3196a452659bb4375e687faf0b09a48ef5557  todo.sr.ht.confd
0735ae57f02c8afec8cc22f8902a5db126b7e9a9ea6f59639d442d6f945b129997429c1e3458725344a8a4d0f5f7644299edff3e88dc70df0db8ce52b5e22853  todo.sr.ht-lmtp.initd
8765703a5ac67da782e471f3f89d0c6fb58cf1dba7342377bccba6e39bd324c47d13948edd5a99af6376b39f60239b3689f6983b92353380f208fe1f12589907  todo.sr.ht-lmtp.confd
befda599f189e99b542ebaf5dd17739169bf73a1499c9de92a98778bd108bb9813e1838e461ced494c6b495a16e34cf805abdb3ccabcf14c2ccecf2d42e780df  todo.sr.ht-webhooks.confd
0e23d03aaf3a435b875716c02f5aa1474e652c5737f12ba8c1a899c3838a9b1ef8d72fc9ad9fa6d98120ba638352a4ad767aae0bb2c1091ad4a6d7a21cb001fb  todo.sr.ht-webhooks.initd
6d096a0559db2c689938e0f0c24d0158efe836dec209f694256f2803494884f206cab423cd5d3b08a68c9446aa2aff1eca0a95da31088fa8a9628becbf344215  todo.sr.ht-api.confd
d2674de8aab929d18f2ff062db935d3531d71cb91a5e7b3cdb5e75ea6a838125097789e442e55d5f6fe512c870fe7a12a327fb7abf834af651265cbea148707f  todo.sr.ht-api.initd
39c12898976aa3b5896ed8287216cf4f2ed4998722891e1b5e507070df86f9111286396eb82cc01b3faaf1016b51db47d077fc4b1b7d72e3782f936c89fb3e37  todo.sr.ht.gunicorn.conf.py
"
